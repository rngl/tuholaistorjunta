

CREATE DATABASE Tuholaistorjunta;
USE Tuholaistorjunta;
CREATE USER 'ttuser'@'shell.metropolia.fi' IDENTIFIED BY 'ttpass';
GRANT SELECT, DELETE, UPDATE, INSERT ON Tuholaistorjunta.* TO 'ttuser'@'shell.metropolia.fi';
FLUSH PRIVILEGES;


CREATE TABLE Tuholainen
(
  Tuholainentunniste INT NOT NULL AUTO_INCREMENT,
  Nimi VARCHAR(40) NOT NULL UNIQUE,
  PRIMARY KEY (Tuholainentunniste)
);

CREATE TABLE Postitoimipaikka
(
  Postinumero VARCHAR(5) NOT NULL,
  Postitoimipaikka VARCHAR(40) NOT NULL,
  PRIMARY KEY (Postinumero)
);

CREATE TABLE Asiakas
(
  Etunimi VARCHAR(40) NOT NULL,
  Sukunimi VARCHAR(40) NOT NULL,
  Puhelin VARCHAR(20) NOT NULL,
  Osoite VARCHAR(50) NOT NULL,
  Asiakastunniste INT NOT NULL AUTO_INCREMENT,
  Sahkoposti VARCHAR(60) NOT NULL UNIQUE,
  Postinumero VARCHAR(5) NOT NULL,
  PRIMARY KEY (Asiakastunniste),
  FOREIGN KEY (Postinumero) REFERENCES Postitoimipaikka(Postinumero)
);

CREATE TABLE Asiakaskaynti
(
  Paivamaara DATE NOT NULL,
  Asiakaskayntitunniste INT NOT NULL AUTO_INCREMENT,
  Asiakastunniste INT NOT NULL,
  PRIMARY KEY (Asiakaskayntitunniste),
  FOREIGN KEY (Asiakastunniste) REFERENCES Asiakas(Asiakastunniste)
);

CREATE TABLE Loytyy
(
  Asiakaskayntitunniste INT NOT NULL,
  Tuholainentunniste INT NOT NULL,
  PRIMARY KEY (Asiakaskayntitunniste, Tuholainentunniste),
  FOREIGN KEY (Asiakaskayntitunniste) REFERENCES Asiakaskaynti(Asiakaskayntitunniste),
  FOREIGN KEY (Tuholainentunniste) REFERENCES Tuholainen(Tuholainentunniste)
);