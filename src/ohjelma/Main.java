/*
 * Staattinen kayttoliittyma
 */

package ohjelma;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Scanner;


public class Main {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		Hakukone hk = new Hakukone();

		int exit = 0;
		do {
			System.out.println("1. Asiakaskaynnit");
			System.out.println("2. Asiakkaat");
			System.out.println("3. Tuholaiset");
			System.out.println("4. Exit");

			exit = parseInt(scanner);

			switch (exit) {
			case 1:
				asiakaskaynnit(scanner, hk);
				break;
			case 2:
				asiakkaat(scanner, hk);
				break;
			case 3:
				tuholaiset(scanner, hk);
				break;
			}
		}
		while (exit != 4);

		scanner.close();
		hk.close();
	}


	public static void asiakkaat(Scanner scanner, Hakukone hk) {
		int exit = 0;
		String sukunimi, etunimi;
		int tunnus;
		do {
			System.out.println("1. Tulosta asiakkaat");
			System.out.println("2. Tulosta asiakaskaynnit ja tuholaiset");
			System.out.println("3. Hae asiakas nimella");
			System.out.println("4. Hae asiakas sahkopostilla");
			System.out.println("5. Hae asiakas tunnisteella");
			System.out.println("6. Lisaa asiakas");
			System.out.println("7. Muokkaa asiakasta");
			System.out.println("8. Poista asiakas");
			System.out.println("9. Postitoimipaikat");
			System.out.println("10. Palaa edelliseen");

			exit = parseInt(scanner);

			switch (exit) {
				case 1:
					tulostaAsiakkaat(hk.haeAsiakas());
					break;
				case 2:
					tulostaAsiakasJaTuholainen2(hk.haeAsiakaskayntiTuholainen());
					//tulostaAsiakasJaTuholainen(hk.haeAsiakasJaTuholaiset());
					break;
				case 3:
					System.out.println("Anna sukunimi:");
					sukunimi = scanner.nextLine();
					System.out.println("Anna etunimi:");
					etunimi = scanner.nextLine();
					tulostaAsiakkaat(hk.haeAsiakas(etunimi, sukunimi));
					break;
				case 4:
					System.out.println("Anna sahkoposti:");
					sukunimi = scanner.nextLine();
					tulostaAsiakkaat(hk.haeAsiakas(sukunimi));
					break;
				case 5:
					System.out.println("Anna tunniste:");
					tunnus = parseInt(scanner);
					tulostaAsiakkaat(hk.haeAsiakas(tunnus));
					break;
				case 6:
					if (hk.lisaaAsiakas(luoAsiakas(scanner)))
						System.out.println("Lisays onnistui");
					else
						System.out.println("Lisays epaonnistui");
					break;
				case 7:
					System.out.println("Anna tunniste:");
					tunnus = parseInt(scanner);
					if (hk.muokkaaAsiakas(tunnus, luoAsiakas(scanner)))
						System.out.println("Muokkaus onnistui");
					else
						System.out.println("Muokkaus epaonnistui");
					break;
				case 8:
					System.out.println("Anna tunniste:");
					tunnus = parseInt(scanner);
					if (hk.poistaAsiakas(tunnus))
						System.out.println("Poisto onnistui");
					else
						System.out.println("Poisto epaonnistui");
					break;
				case 9:
					postitoimipaikat(scanner, hk);
					break;
			}
		}
		while (exit != 10);
	}

	public static void postitoimipaikat(Scanner scanner, Hakukone hk) {
		int exit = 0;
		String numero, paikka;
		do {
			System.out.println("1. Tulosta postitoimipaikat");
			System.out.println("2. Hae postitoimipaikka");
			System.out.println("3. Lisaa postitoimipaikka");
			System.out.println("4. Muokkaa postitoimipaikkaa");
			System.out.println("5. Poista postitoimipaikka");
			System.out.println("6. Palaa edelliseen");

			exit = parseInt(scanner);

			switch (exit) {
				case 1:
					tulostaPostitoimipaikat(hk.haePostitoimipaikka());
					break;
				case 2:
					System.out.println("Anna postinumero:");
					numero = scanner.nextLine();
					tulostaPostitoimipaikat(hk.haePostitoimipaikka(numero));
					break;
				case 3:
					System.out.println("Anna postinumero:");
					numero = scanner.nextLine();
					System.out.println("Anna postitoimipaikka:");
					paikka = scanner.nextLine();
					if (hk.lisaaPostitoimipaikka(numero, paikka))
						System.out.println("Lisays onnistui");
					else
						System.out.println("Lisays epaonnistui");
					break;
				case 4:
					System.out.println("Anna postinumero:");
					numero = scanner.nextLine();
					System.out.println("Anna uusi postitoimipaikka:");
					paikka = scanner.nextLine();
					if (hk.muokkaaPostitoimipaikka(numero, paikka))
						System.out.println("Muokkaus onnistui");
					else
						System.out.println("Muokkaus epaonnistui");
					break;
				case 5:
					System.out.println("Anna postinumero");
					numero = scanner.nextLine();
					if (hk.poistaPostitoimipaikka(numero))
						System.out.println("Poisto onnistui");
					else
						System.out.println("Poisto epaonnistui");
					break;
			}
		}
		while (exit != 6);
	}

	public static Asiakas luoAsiakas(Scanner scanner) {

		String[] arvo = new String[7];

		System.out.println("Anna etunimi:");
		arvo[0] = scanner.nextLine();
		System.out.println("Anna sukunimi:");
		arvo[1] = scanner.nextLine();
		System.out.println("Anna osoite:");
		arvo[2] = scanner.nextLine();
		System.out.println("Anna postinumero:");
		arvo[3] = scanner.nextLine();
		System.out.println("Anna postitoimipaikka:");
		arvo[4] = scanner.nextLine();
		System.out.println("Anna sahkoposti:");
		arvo[5] = scanner.nextLine();
		System.out.println("Anna puhelin:");
		arvo[6] = scanner.nextLine();

		// muokataan lyhyet vastaukset (esim enter) null arvoiksi
		for (int i = 0; i < arvo.length; i++) {
			if (arvo[i].length() < 2)
				arvo[i] = null;
		}

		return new Asiakas(arvo[0], arvo[1], arvo[2], arvo[6], arvo[5],
				new Postitoimipaikka(arvo[3], arvo[4]));
	}

	public static void asiakaskaynnit(Scanner scanner, Hakukone hk) {
		int exit = 0;
		do {
			System.out.println("1. Tulosta asiakaskaynnit");
			System.out.println("2. Tulosta asiakaskaynnit ja tuholaiset");
			System.out.println("3. Lisaa asiakaskaynti");
			System.out.println("4. Poista asiakaskaynti");
			System.out.println("5. Lisaa tuholainen asiakaskayntiin");
			System.out.println("6. Poista tuholainen asiakaskaynnista");
			System.out.println("7. Palaa edelliseen");

			exit = parseInt(scanner);

			switch (exit) {
				case 1:
					tulostaAsiakaskaynnit(hk.haeAsiakaskaynti());
					break;
				case 2:
					tulostaAsiakaskaynnitTuholaiset(hk.haeAsiakaskayntiTuholainen());
					break;
				case 3:
					lisaaAsiakaskaynti(scanner, hk);
					break;
				case 4:
					poistaAsiakaskaynti(scanner, hk);
					break;
				case 5:
					lisaaTuholainenAsiakaskayntiin(scanner, hk);
					break;
				case 6:
					poistaTuholainenAsiakaskaynnista(scanner, hk);
					break;

			}
		}
		while (exit != 7);
	}

	public static void tuholaiset(Scanner scanner, Hakukone hk) {
		int exit = 0;
		String nimi;
		int tunnus;
		do {
			System.out.println("1. Tulosta tuholaiset");
			System.out.println("2. Hae tuholainen nimella");
			System.out.println("3. Lisaa tuholainen");
			System.out.println("4. Muokkaa tuholaista");
			System.out.println("5. Poista tuholainen");
			System.out.println("6. Palaa edelliseen");

			exit = parseInt(scanner);

			switch (exit) {
				case 1:
					tulostaTuholaiset(hk.haeTuholainen());
					break;
				case 2:
					System.out.println("Anna nimi:");
					nimi = scanner.nextLine();
					tulostaTuholaiset(hk.haeTuholainen(nimi));
					break;
				case 3:
					System.out.println("Anna nimi:");
					nimi = scanner.nextLine();
					if (hk.lisaaTuholainen(nimi))
						System.out.println("Lisays onnistui");
					else
						System.out.println("Lisays epaonnistui");
					break;
				case 4:
					System.out.println("Anna tuholaistunniste:");
					tunnus = parseInt(scanner);
					System.out.println("Anna uusi nimi:");
					nimi = scanner.nextLine();
					if (hk.muokkaaTuholainen(tunnus, nimi))
						System.out.println("Muokkaus onnistui");
					else
						System.out.println("Muokkaus epaonnistui");
					break;

				case 5:
					System.out.println("Anna tuholaistunniste");
					tunnus = parseInt(scanner);
					if (hk.poistaTuholainen(tunnus))
						System.out.println("Poisto onnistui");
					else
						System.out.println("Poisto epaonnistui");
					break;
			}
		}
		while (exit != 6);
	}

	public static void poistaTuholainenAsiakaskaynnista(Scanner scanner, Hakukone hk) {
		System.out.println("Anna asiakaskayntitunniste:");
		int atunnus = parseInt(scanner);
		System.out.println("Anna tuholaistunniste:");
		int ttunnus = parseInt(scanner);
		if (hk.poistaTuholainenAsiakaskaynnista(atunnus, ttunnus))
			System.out.println("Poisto onnistui");
		else
			System.out.println("Poisto epaonnistui");
	}


	public static void lisaaTuholainenAsiakaskayntiin(Scanner scanner, Hakukone hk) {
		System.out.println("Anna asiakaskayntitunniste:");
		int atunniste = parseInt(scanner);
		System.out.println("Anna tuholaistunniste:");
		int ttunniste = parseInt(scanner);
		if (hk.lisaaTuholainenAsiakaskayntiin(atunniste, ttunniste))
			System.out.println("Lisays onnistui");
		else
			System.out.println("Lisays epaonnistui");
	}

	public static void poistaAsiakaskaynti(Scanner scanner, Hakukone hk) {
		System.out.println("Anna asiakaskayntitunniste:");
		int atunniste = parseInt(scanner);
		if (hk.poistaAsiakaskaynti(atunniste))
			System.out.println("Poisto onnistui");
		else
			System.out.println("Poisto epaonnistui");
	}

	public static void lisaaAsiakaskaynti(Scanner scanner, Hakukone hk) {
		LocalDate ld = null;

		do {
		System.out.println("Anna paivamaara (dd/M/yyyy):");
		String pvm = scanner.nextLine();
			try {
				ld = LocalDate.parse(pvm,DateTimeFormatter.ofPattern("dd/M/yyyy"));
			}
			catch(DateTimeParseException e) {
				System.out.println("Vaara muoto paivamaarassa!");
			}
		}
		while (ld == null);

		System.out.println("Anna asiakastunniste:");
		int atunniste = parseInt(scanner);

		System.out.println("Anna tuholaistunniste:");
		int[] ttunniste = new int[1]; // tuholaisia voisi lisata montakin mutta ei talla kertaa
		ttunniste[0] = parseInt(scanner);

		if (hk.lisaaAsiakaskaynti(atunniste, ld, ttunniste))
			System.out.println("Lisays onnistui.");
		else
			System.out.println("Lisays epaonnistui.");
	}

	// tulostaa asiakaskaynnit ja tuholaiset
	public static void tulostaAsiakaskaynnitTuholaiset(List<Asiakaskaynti> asiakaskaynnit) {
		System.out.printf("Asiakaskayntitunniste\tAsiakastunniste\tPaivamaara\tTuholaiset\n");
		for (Asiakaskaynti a: asiakaskaynnit) {
			System.out.printf("\t%d\t\t\t%d\t%s",
					a.getAsiakaskayntitunniste(), a.getAsiakas().getAsiakastunniste(),
					a.getPaivamaara().toString());
			for (Tuholainen t: a.getTuholaiset())
				System.out.printf("\t%s", t.getNimi());
			System.out.println("");
		}
	}

	public static void tulostaAsiakaskaynnit(List<Asiakaskaynti> asiakaskaynnit) {
		System.out.printf("Asiakaskayntitunniste\tAsiakastunniste\tPaivamaara\n");
		for (Asiakaskaynti a: asiakaskaynnit)
			System.out.printf("\t%d\t\t\t%d\t%s\n",
					a.getAsiakaskayntitunniste(), a.getAsiakas().getAsiakastunniste(),
					a.getPaivamaara().toString());
	}

	public static void tulostaAsiakkaat(List<Asiakas> asiakkaat) {
		System.out.printf("Asiakastunniste\tSukunimi\tEtunimi\tPuhelin\t\tSahkoposti\tOsoite\tPostinumero\tPostitoimipaikka\n");
		for (Asiakas a: asiakkaat)
			System.out.printf("\t%d\t%s\t\t%s\t%s\t%s\t%s\t%s\t\t%s\n",
					a.getAsiakastunniste(), a.getSukunimi(), a.getEtunimi(),
					a.getPuhelin(), a.getSahkoposti(), a.getOsoite(),
					a.getPostitoimipaikka().getPostinumero(), a.getPostitoimipaikka().getPostitoimipaikka());
	}

	public static void tulostaTuholaiset(List<Tuholainen> tuholaiset) {
		System.out.printf("Tuholaistunniste\tNimi\n");
		for (Tuholainen t: tuholaiset)
			System.out.printf("\t%d\t\t%s\n",
					t.getTuholainentunniste(), t.getNimi());
	}

	// ensimmainen versio
	public static void tulostaAsiakasJaTuholainen(List<Object[]> lista) {
		System.out.printf("Etunimi\tSukunimi\tAsiakastunniste\tAsiakaskayntitunniste\tTuholainen\n");
		for (Object[] o: lista)
			System.out.printf("%s\t%s\t\t\t%d\t\t%d\t\t%s\n", o[0], o[1], o[2], o[3], o[4]);
	}

	// toinen versio
	public static void tulostaAsiakasJaTuholainen2(List<Asiakaskaynti> lista) {
		System.out.printf("Etunimi\tSukunimi\tAsiakastunniste\tAsiakaskayntitunniste\tTuholainen\n");
		for (Asiakaskaynti a: lista) {
			System.out.printf("%s\t%s\t\t\t%d\t\t%d\t",
					a.getAsiakas().getEtunimi(), a.getAsiakas().getSukunimi(),
					a.getAsiakas().getAsiakastunniste(), a.getAsiakaskayntitunniste());
			for (Tuholainen t: a.getTuholaiset())
				System.out.printf("\t%s", t.getNimi());
			System.out.println("");
		}
	}

	public static void tulostaPostitoimipaikat(List<Postitoimipaikka> lista) {
		System.out.printf("\tPostinumero\tPostitoimipaikka\n");
		for (Postitoimipaikka p: lista)
			System.out.printf("\t%s\t\t%s\n",
					p.getPostinumero(), p.getPostitoimipaikka());
	}

	// kysyy int kunnes saa
	public static int parseInt(Scanner scanner) {
		int i;
		while (!scanner.hasNextInt()) scanner.next();
		i = scanner.nextInt();
		scanner.nextLine();
		return i;
	}
}
