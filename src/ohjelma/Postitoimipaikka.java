package ohjelma;

import javax.persistence.*;

@Entity
@Table(name="Postitoimipaikka")
public class Postitoimipaikka {

	@Id
	@Column(name="Postinumero", nullable=false)
	private String postinumero;
	@Column(name="Postitoimipaikka", nullable=false)
	private String postitoimipaikka;

	public Postitoimipaikka() {}

	public Postitoimipaikka(String numero, String paikka) {
		postinumero = numero;
		postitoimipaikka = paikka;
	}

	public String getPostinumero() {
		return postinumero;
	}

	public void setPostinumero(String postinumero) {
		this.postinumero = postinumero;
	}

	public String getPostitoimipaikka() {
		return postitoimipaikka;
	}

	public void setPostitoimipaikka(String postitoimipaikka) {
		this.postitoimipaikka = postitoimipaikka;
	}
}
