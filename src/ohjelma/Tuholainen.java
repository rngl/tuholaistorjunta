package ohjelma;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="Tuholainen")
public class Tuholainen {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Tuholainentunniste")
	private int tuholainentunniste;
	@Column(name="Nimi", nullable=false)
	private String nimi;

	@ManyToMany(cascade=CascadeType.PERSIST, mappedBy="tuholaiset")
	private List<Asiakaskaynti> asiakaskaynnit;

	public Tuholainen() {
		asiakaskaynnit = new ArrayList<Asiakaskaynti>();
	}

	public Tuholainen(String nimi) {
		asiakaskaynnit = new ArrayList<Asiakaskaynti>();
		this.nimi = nimi;
	}

	public void lisaaAsiakaskaynti(Asiakaskaynti ak) {
		asiakaskaynnit.add(ak);
	}

	public int getTuholainentunniste() {
		return tuholainentunniste;
	}

	public void setTuholainentunniste(int tuholainentunniste) {
		this.tuholainentunniste = tuholainentunniste;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public List<Asiakaskaynti> getAsiakaskaynnit() {
		return asiakaskaynnit;
	}

	public void setAsiakaskaynnit(List<Asiakaskaynti> asiakaskaynnit) {
		this.asiakaskaynnit = asiakaskaynnit;
	}



}
