package ohjelma;

import javax.persistence.*;

@Entity
@Table(name="Asiakas")
public class Asiakas {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Asiakastunniste", nullable=false)
	private int asiakastunniste;
	@Column(name="Sukunimi", nullable=false)
	private String sukunimi;
	@Column(name="Etunimi", nullable=false)
	private String etunimi;
	@Column(name="Osoite", nullable=true)
	private String osoite;
	@Column(name="Puhelin", nullable=true)
	private String puhelin;
	@Column(name="Sahkoposti", nullable=true)
	private String sahkoposti;
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="Postinumero", nullable=false)
	private Postitoimipaikka postitoimipaikka;

	public Asiakas() {}

	public Asiakas(String enimi, String snimi, String soite, String puh, String sposti, Postitoimipaikka ptp) {
		etunimi = enimi;
		sukunimi = snimi;
		osoite = soite;
		puhelin = puh;
		sahkoposti = sposti;
		postitoimipaikka = ptp;
	}

	public boolean puuttuuArvoja() {
		if (sukunimi == null || etunimi == null || osoite == null || puhelin == null
				|| sahkoposti == null || postitoimipaikka == null)
			return true;

		if (postitoimipaikka.getPostinumero() == null ||
				postitoimipaikka.getPostitoimipaikka() == null)
			return true;

		return false;
	}

	public int getAsiakastunniste() {
		return asiakastunniste;
	}

	public void setAsiakastunniste(int asiakastunniste) {
		this.asiakastunniste = asiakastunniste;
	}

	public String getSukunimi() {
		return sukunimi;
	}

	public void setSukunimi(String sukunimi) {
		this.sukunimi = sukunimi;
	}

	public String getEtunimi() {
		return etunimi;
	}

	public void setEtunimi(String etunimi) {
		this.etunimi = etunimi;
	}

	public String getOsoite() {
		return osoite;
	}

	public void setOsoite(String osoite) {
		this.osoite = osoite;
	}

	public String getPuhelin() {
		return puhelin;
	}

	public void setPuhelin(String puhelin) {
		this.puhelin = puhelin;
	}

	public String getSahkoposti() {
		return sahkoposti;
	}

	public void setSahkoposti(String sahkoposti) {
		this.sahkoposti = sahkoposti;
	}

	public Postitoimipaikka getPostitoimipaikka() {
		return postitoimipaikka;
	}

	public void setPostitoimipaikka(Postitoimipaikka postitoimipaikka) {
		this.postitoimipaikka = postitoimipaikka;
	}


}
