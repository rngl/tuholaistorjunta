package ohjelma;

import javax.persistence.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name="Asiakaskaynti")
public class Asiakaskaynti {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Asiakaskayntitunniste")
	private int asiakaskayntitunniste;
	@Column(name="Paivamaara", nullable=false)
	private LocalDate paivamaara;
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="Asiakastunniste", nullable=false)
	private Asiakas asiakas;

	@ManyToMany(cascade=CascadeType.PERSIST)
	@JoinTable(name="Loytyy",
			joinColumns = {@JoinColumn(name="Asiakaskayntitunniste")},
			inverseJoinColumns = {@JoinColumn(name="Tuholainentunniste")})
	private List<Tuholainen> tuholaiset;


	public Asiakaskaynti() {
		tuholaiset = new ArrayList<Tuholainen>();
	}

	public Asiakaskaynti(LocalDate pvm, Asiakas as) {
		paivamaara = pvm;
		asiakas = as;
		tuholaiset = new ArrayList<Tuholainen>();
	}

	public void lisaaTuholainen(Tuholainen t) {
		tuholaiset.add(t);
	}

	public int getAsiakaskayntitunniste() {
		return asiakaskayntitunniste;
	}

	public void setAsiakaskayntitunniste(int asiakaskayntitunniste) {
		this.asiakaskayntitunniste = asiakaskayntitunniste;
	}

	public LocalDate getPaivamaara() {
		return paivamaara;
	}

	public void setPaivamaara(LocalDate paivamaara) {
		this.paivamaara = paivamaara;
	}

	public Asiakas getAsiakas() {
		return asiakas;
	}

	public void setAsiakas(Asiakas asiakas) {
		this.asiakas = asiakas;
	}

	public List<Tuholainen> getTuholaiset() {
		return tuholaiset;
	}

	public void setTuholaiset(List<Tuholainen> tuholaiset) {
		this.tuholaiset = tuholaiset;
	}

}
