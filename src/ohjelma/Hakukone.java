/*
 * Hakukone suorittaa kyselyt Tuholaistorjunta-tietokantaan
 *
 */

package ohjelma;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

public class Hakukone {

	private EntityManagerFactory emf;
	private EntityManager em;

	public Hakukone() {
		emf = Persistence.createEntityManagerFactory("Tuholaistorjunta");
		em = emf.createEntityManager();
	}

	public void close() {
		em.close();
		emf.close();
	}

	public boolean lisaaAsiakas(Asiakas uusi) {
		if (uusi.puuttuuArvoja())
			return false;
		em.clear();
		em.getTransaction().begin();

		Postitoimipaikka p = em.find(Postitoimipaikka.class, uusi.getPostitoimipaikka().getPostinumero());
		if (p != null)
			uusi.setPostitoimipaikka(p);
		try {
			em.persist(uusi);
		}
		catch (PersistenceException e) {

		}

		try {
			em.getTransaction().commit();
			return true;
		}
		catch(RollbackException e) {
			return false;
		}	}

	@SuppressWarnings("unchecked")
	public List<Asiakas> haeAsiakas(String etunimi, String sukunimi) {
		em.clear();
		return em.createQuery("SELECT a FROM Asiakas a "
				+ "WHERE etunimi = :en AND sukunimi = :sn ORDER BY a.asiakastunniste")
                .setParameter("en", etunimi)
                .setParameter("sn", sukunimi)
                .getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Asiakas> haeAsiakas(String sahkoposti) {
		em.clear();
		return em.createQuery("SELECT a FROM Asiakas a "
				+ "WHERE sahkoposti = :sp")
                .setParameter("sp", sahkoposti)
                .getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Asiakas> haeAsiakas(int tunnus) {
		em.clear();
		return em.createQuery("SELECT a FROM Asiakas a "
				+ "WHERE asiakastunniste = :id")
                .setParameter("id", tunnus)
                .getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Asiakas> haeAsiakas() {
		em.clear();
		return em.createQuery("SELECT a FROM Asiakas a ORDER BY a.asiakastunniste")
                .getResultList();
	}

	public boolean poistaAsiakas(int tunnus) {
		em.clear();
		em.getTransaction().begin();
		int tulos = 0;
		/* poistaa liikaa..
		try {
			tulos = em.createQuery("DELETE FROM Asiakas a "
					+ "WHERE asiakastunniste = :id")
					.setParameter("id", tunnus).executeUpdate();
		}
		catch(PersistenceException e) {

		}*/

		Asiakas a = em.find(Asiakas.class, tunnus);
		if (a != null) {
			em.remove(a);
			tulos = 1;
		}

		try {
			em.getTransaction().commit();
			if (tulos == 1)
				return true;
			else
				return false;
		}
		catch(RollbackException e) {
			return false;
		}
	}

	/*
	 *
	 * Muokkaa tunnuksella haetun asiakkaan tietoja vastaamaan
	 * asiakas uuden tietoja. Tyhjia tietoja (null) ei muuteta.
	 */
	public boolean muokkaaAsiakas(int tunnus, Asiakas uusi) {
		em.clear();
		em.getTransaction().begin();

		Asiakas a = em.find(Asiakas.class, tunnus);

		if (a == null) {
			em.getTransaction().rollback();
			return false;
		}

		if (uusi.getEtunimi() != null)
			a.setEtunimi(uusi.getEtunimi());
		if (uusi.getSukunimi() != null)
			a.setSukunimi(uusi.getSukunimi());
		if (uusi.getOsoite() != null)
			a.setOsoite(uusi.getOsoite());
		if (uusi.getPuhelin() != null)
			a.setPuhelin(uusi.getPuhelin());
		if (uusi.getSahkoposti() != null)
			a.setSahkoposti(uusi.getSahkoposti());

		/*
		 * Postitoimipaikan muuttaminen on monimutkainen homma:
		 * Jos loytyy muutettavalla pnumerolla entuudestaan,
		 * niin vaihdetaan pnumero siihen (ja paikkakunta).
		 * Tassa tapauksessa jos paikkakunta on annettu kanssa,
		 * niin sita ei muuteta vaikka olisi eri kuin mita tkannassa.
		 *
		 * Jos ei loydy entuudestaan, lisataan jos paikkakunta on myos annettu.
		 *
		 * feature: Jos pelkka paikkakunta annetaan, eika numeroa, niin muutetaan nykyisen
		 * postinumeron paikkakunta annettuun paikkakuntaan. Vaikuttaa kaikkiin joilla
		 * sama numero.
		 */
		if (uusi.getPostitoimipaikka().getPostinumero() != null) {
			Postitoimipaikka p = em.find(Postitoimipaikka.class, uusi.getPostitoimipaikka().getPostinumero());
			if (p != null && p.getPostinumero() != a.getPostitoimipaikka().getPostinumero())
				a.setPostitoimipaikka(p);
			else if (p == null && uusi.getPostitoimipaikka().getPostitoimipaikka() != null) {
				p = uusi.getPostitoimipaikka();
				em.persist(p);
				a.setPostitoimipaikka(p);
			}
		}
		else if (uusi.getPostitoimipaikka().getPostitoimipaikka() != null)
			a.getPostitoimipaikka().setPostitoimipaikka(uusi.getPostitoimipaikka().getPostitoimipaikka());

		em.persist(a);
		try {
			em.getTransaction().commit();
			return true; // true myos vaikkei mitaan muuttuisikaan kaytannossa (ei virhetta)
		}
		catch(RollbackException e) {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Asiakaskaynti> haeAsiakaskaynti(int tunnus) {
		em.clear();
		return em.createQuery("SELECT a FROM Asiakaskaynti a "
				+ "WHERE asiakastunniste = :id ORDER BY a.asiakaskayntitunniste")
                .setParameter("id", tunnus)
                .getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Asiakaskaynti> haeAsiakaskaynti(int tunnus, int i) { //dummy parametri i
		em.clear();
		return em.createQuery("SELECT a FROM Asiakaskaynti a "
				+ "WHERE asiakaskayntitunniste = :id ORDER BY a.asiakaskayntitunniste")
                .setParameter("id", tunnus)
                .getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Asiakaskaynti> haeAsiakaskaynti(LocalDate pvm) {
		em.clear();
		return em.createQuery("SELECT a FROM Asiakaskaynti a "
				+ "WHERE paivamaara = :id ORDER BY a.asiakaskayntitunniste")
                .setParameter("id", pvm)
                .getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Asiakaskaynti> haeAsiakaskaynti() {
		em.clear();
		return em.createQuery("SELECT a FROM Asiakaskaynti a ORDER BY a.asiakaskayntitunniste")
                .getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Asiakaskaynti> haeAsiakaskayntiTuholainen() {
		em.clear();
		return em.createQuery("SELECT DISTINCT a FROM Asiakaskaynti a JOIN FETCH a.tuholaiset"
				+ " JOIN FETCH a.asiakas ORDER BY a.asiakaskayntitunniste")
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> haeAsiakasJaTuholaiset() {
		em.clear();
		return em.createQuery("SELECT a.etunimi, a.sukunimi, a.asiakastunniste, "
				+ "ak.asiakaskayntitunniste, t.nimi FROM Asiakaskaynti ak JOIN ak.asiakas a "
				+ "JOIN ak.tuholaiset t ORDER BY ak.asiakaskayntitunniste")
				.getResultList();
	}

	public boolean lisaaAsiakaskaynti(int tunnus, LocalDate pvm, int[] tuholaiset) {
		em.clear();
		em.getTransaction().begin();

		boolean ret = false;
		Asiakas a = em.find(Asiakas.class, tunnus);
		Tuholainen t;
		List<Tuholainen> tl = new ArrayList<Tuholainen>();
		for (int i: tuholaiset) {
			t = em.find(Tuholainen.class, i);
			if (t != null)
				tl.add(t);
		}

		if (a != null) {
			Asiakaskaynti ak = new Asiakaskaynti(pvm, a);
			ak.setTuholaiset(tl);
			tl.forEach((tt)->{tt.lisaaAsiakaskaynti(ak);em.persist(tt);});
			em.persist(ak);
			ret = true;
		}

		try {
			em.getTransaction().commit();
			return ret;
		}
		catch(RollbackException e) {
			return false;
		}
	}

	public boolean poistaAsiakaskaynti(int tunnus) {
		em.clear();
		em.getTransaction().begin();
		int tulos = em.createQuery("DELETE FROM Asiakaskaynti a "
				+ "WHERE asiakaskayntitunniste = :id")
				.setParameter("id", tunnus).executeUpdate();
		try {
			em.getTransaction().commit();
			if (tulos == 1)
				return true;
			else
				return false;
		}
		catch(RollbackException e) {
			return false;
		}
	}

	public boolean lisaaTuholainen(String nimi) {
		em.clear();
		em.getTransaction().begin();

		Tuholainen t = new Tuholainen(nimi);
		try {
			em.persist(t);
		}
		catch (PersistenceException e) {

		}

		try {
			em.getTransaction().commit();
			return true;
		}
		catch(RollbackException e) {
			return false;
		}
	}

	public boolean muokkaaTuholainen(int tunnus, String nimi) {
		boolean ret = false;
		em.clear();
		em.getTransaction().begin();

		Tuholainen t = em.find(Tuholainen.class, tunnus);
		if (t != null) {
			t.setNimi(nimi);
			em.persist(t);
			ret = true;
		}

		try {
			em.getTransaction().commit();
			return ret;
		}
		catch(RollbackException e) {
			return false;
		}
	}

	public boolean poistaTuholainen(int tunnus) {
		em.clear();
		em.getTransaction().begin();
		int tulos = 0;
		/* dangerous
		int tulos = em.createQuery("DELETE FROM Tuholainen t "
				+ "WHERE tuholainentunniste = :id")
				.setParameter("id", tunnus).executeUpdate();
				*/
		Tuholainen t = em.find(Tuholainen.class, tunnus);
		if (t != null) {
			em.remove(t);
			tulos = 1;
		}

		try {
			em.getTransaction().commit();
			if (tulos == 1)
				return true;
			else
				return false;
		}
		catch(RollbackException e) {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Tuholainen> haeTuholainen(String nimi) {
		em.clear();
		return em.createQuery("SELECT t FROM Tuholainen t "
				+ "WHERE nimi LIKE :id ORDER BY t.tuholainentunniste")
                .setParameter("id", "%"+nimi+"%")
                .getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Tuholainen> haeTuholainen() {
		em.clear();
		return em.createQuery("SELECT t FROM Tuholainen t ORDER BY t.tuholainentunniste")
                .getResultList();
	}

	public boolean poistaTuholainenAsiakaskaynnista(int aktunnus, int ttunnus) {
		em.clear();
		em.getTransaction().begin();

		boolean ret = false;

		Asiakaskaynti ak = em.find(Asiakaskaynti.class, aktunnus);
		Tuholainen t = em.find(Tuholainen.class, ttunnus);
		if (ak != null && t != null) {

			if (ak.getTuholaiset().size() > 1)
				if (ak.getTuholaiset().remove(t))
					if (t.getAsiakaskaynnit().remove(ak)) {
						em.merge(ak);
						ret = true;
					}
		}

		try {
			em.getTransaction().commit();
			return ret;
		}
		catch(RollbackException e) {
			return false;
		}
	}


	public boolean lisaaTuholainenAsiakaskayntiin(int aktunnus, int ttunnus) {
		em.clear();
		em.getTransaction().begin();

		boolean ret = false;

		Asiakaskaynti ak = em.find(Asiakaskaynti.class, aktunnus);
		Tuholainen t = em.find(Tuholainen.class, ttunnus);
		if (ak != null && t != null) {
			ak.lisaaTuholainen(t);
			t.lisaaAsiakaskaynti(ak);

			em.merge(ak);
			ret = true;
		}

		try {
			em.getTransaction().commit();
			return ret;
		}
		catch(RollbackException e) {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Postitoimipaikka> haePostitoimipaikka() {
		em.clear();
		return em.createQuery("SELECT p FROM Postitoimipaikka p ORDER BY postinumero")
                .getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Postitoimipaikka> haePostitoimipaikka(String tunnus) {
		em.clear();
		return em.createQuery("SELECT p FROM Postitoimipaikka p "
				+ "WHERE postinumero = :id ORDER BY postinumero")
                .setParameter("id", tunnus)
                .getResultList();
	}


	public boolean lisaaPostitoimipaikka(String numero, String paikka) {
		if (numero.length() != 5 || paikka.length() < 2)
			return false;
		em.clear();
		em.getTransaction().begin();

		Postitoimipaikka p = new Postitoimipaikka(numero, paikka);
		em.persist(p);

		try {
			em.getTransaction().commit();
			return true;
		}
		catch(RollbackException e) {
			return false;
		}
	}

	public boolean muokkaaPostitoimipaikka(String numero, String paikka) {
		if (numero.length() != 5 || paikka.length() < 2)
			return false;

		boolean ret = false;
		em.clear();
		em.getTransaction().begin();

		Postitoimipaikka p = em.find(Postitoimipaikka.class, numero);
		if (p != null) {
			p.setPostitoimipaikka(paikka);
			em.persist(p);
			ret = true;
		}

		try {
			em.getTransaction().commit();
			return ret;
		}
		catch(RollbackException e) {
			return false;
		}
	}

	public boolean poistaPostitoimipaikka(String numero) {
		em.clear();
		em.getTransaction().begin();
		int tulos = 0;
		Postitoimipaikka p = em.find(Postitoimipaikka.class, numero);
		if (p != null) {
			em.remove(p);
			tulos = 1;
		}

		try {
			em.getTransaction().commit();
			if (tulos == 1)
				return true;
			else
				return false;
		}
		catch(RollbackException e) {
			return false;
		}
	}
}
